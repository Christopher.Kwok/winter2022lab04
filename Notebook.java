public class Notebook{
	
	private String brand;
	private String color;
	private int numOfPages;
	
	public Notebook(String brand, String color, int numOfPages){
		this.brand = brand;
		this.color = color;
		this.numOfPages = numOfPages;
	} 
	
	public String getBrand(){
		return this.brand;
	}
	
	public String getColor(){
		return this.color;
	}
	
	public void setColor(String newColor){
		this.color = newColor;
	}
	
	public int getNumOfPages(){
		return this.numOfPages;
	}
	
	public void setNumOfPages(int newNumOfPages){
		this.numOfPages = newNumOfPages;
	}
	
	
	public void ripPage(){
		if(numOfPages > 0){
			this.numOfPages--;
			System.out.println("You ripped a page out of the notebook! There are " + this.numOfPages + " left!");
		}else{ //Added so that it cannot rip into negative numbers.			
			System.out.println("There are no more pages to rip out!");
		}
	}
	
}