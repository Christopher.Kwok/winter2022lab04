import java.util.Scanner;

public class Shop{
	
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		String brand;
		String coverColor;
		int numOfPages = 0;
		
		Notebook[] shelfOne = new Notebook[4];
		
		for(int i = 0; i < 4; i++){
			
			System.out.println("What brand of notebook is it?");
			brand = sc.nextLine();
			System.out.println("What color is the cover of the notebook?");
			coverColor = sc.nextLine();
			System.out.println("How many pages does this notebook have?");
			numOfPages = Integer.parseInt(sc.nextLine());
			System.out.println(""); //To create a separation between notebook inputs.
						
			shelfOne[i] = new Notebook(brand, coverColor, numOfPages);
			
		}
		
		
		//To grab the last item in my array I an using index positio [shelfOne.length-1] so that I can have an array of any length.
		System.out.print("The last notebook is from the brand " + shelfOne[shelfOne.length-1].getBrand());
		System.out.print(" with a " + shelfOne[shelfOne.length-1].getColor() + " cover and has ");
		System.out.println(shelfOne[shelfOne.length-1].getNumOfPages() + " pages.");
		System.out.println(""); //To create a separation.
		
		System.out.println("What is the new cover color of the notebook?");
		coverColor = sc.nextLine();
		System.out.println("How many pages does this notebook have now?");
		numOfPages = Integer.parseInt(sc.nextLine());
		
		//Updating the last notebook with new user inputs
		shelfOne[shelfOne.length-1].setColor(coverColor);
		shelfOne[shelfOne.length-1].setNumOfPages(numOfPages);
		System.out.println("");
		
		System.out.print("The updated notebook is from the brand " + shelfOne[shelfOne.length-1].getBrand());
		System.out.print(" with a " + shelfOne[shelfOne.length-1].getColor() + " cover and has ");
		System.out.println(shelfOne[shelfOne.length-1].getNumOfPages() + " pages.");
		System.out.println("");
		
		//Running ripPage() 5 times to show it updates properly and can keep working.
		shelfOne[shelfOne.length-1].ripPage();
		shelfOne[shelfOne.length-1].ripPage();
		shelfOne[shelfOne.length-1].ripPage();
		shelfOne[shelfOne.length-1].ripPage();
		shelfOne[shelfOne.length-1].ripPage();
	}
	
}